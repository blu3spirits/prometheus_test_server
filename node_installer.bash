#!/usr/bin/bash
cd /tmp
curl -LO https://github.com/prometheus/node_exporter/releases/download/v0.18.0/node_exporter-0.18.0.linux-amd64.tar.gz
tar xvf node_exporter*
mv node_exporter*/node_exporter /usr/local/bin/
useradd -rs /bin/false node_exporter
cat << '__EOF__' > /etc/systemd/system/node_exporter.service
[Unit]
Description=Node Exporter
After=network.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
__EOF__
systemctl daemon-reload
systemctl start node_exporter
systemctl status node_exporter
systemctl enable node_exporter
firewall-cmd --permanent --add-port 9100/tcp
firewall-cmd --reload
rm -rf node_exporter-0.18.0.linux-amd64*
